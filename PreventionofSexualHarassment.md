# Prevention of Sexual Harassment



## 1. Understanding Sexual Harassment

Sexual harassment encompasses a spectrum of behaviors, including:

* Verbal Harassment: Such as making sexually suggestive comments, jokes, or propositions.
* Non-verbal Harassment: Including leering, gestures, or displaying sexually explicit materials.
* Physical Harassment: Involving unwanted physical contact, ranging from touching to assault.
* Quid Pro Quo Harassment: Conditioning employment benefits on sexual favors or threats of repercussions for refusal.
* Cyber Harassment: Transcending physical boundaries, comprising unsolicited sexual messages or images disseminated electronically.

## 2. Addressing Incidents of Sexual Harassment

In confronting or observing instances of sexual harassment, prompt action is imperative:

* Report the Incident: Utilize established channels within the organization, such as HR departments or supervisors.
* Confronting the harasser: If you feel safe to do so, it is recommended to directly address the harasser and describe the behavior you find unacceptable and request that it cease.
* Extend Support: Offer empathetic support to the victim, lending an attentive ear and facilitating access to support resources.
* Adhere to Protocols: Follow organizational procedures for handling complaints, which may entail investigations and disciplinary measures against perpetrators.
* Promote proactive bystander intervention: Advocate for active bystander involvement, urging individuals to intervene or speak out against harassment and provide solidarity to victims.
* Promote Cultural Shifts: Advocate for policies and practices fostering environments of respect, equality, and zero tolerance towards sexual harassment.



## Reference

* [Sexual Harassment Overview by 
Avanta Small Biz Compliance ](https://www.youtube.com/watch?v=Ue3BTGW3uRQ)
* [Bullying and Harassment Mean for You and Your Workplace](https://www.youtube.com/watch?v=u7e2c6v1oDs)
  
* [EMPLOYMENT SEXUAL HARASSMENT by 
Cordua Training Videos ](https://www.youtube.com/watch?v=o3FhoCz-FbA)