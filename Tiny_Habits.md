# Tiny Habits

## The most interesting idea by BJ Fogg:

Tiny habits are very small, easy behaviors that you can start with to achieve significant behavior change. 

The speaker, BJ Fogg, argues that these tiny habits are so easy that they require minimal motivation and willpower to perform. The key to this method is pairing the new behavior with an existing habit that you already perform regularly. For example, doing two push-ups after you use the restroom. By pairing the new behavior with an existing one, the existing behavior serves as a trigger or reminder to perform the new behavior.

## How to use B = MAP to make making new habits easier? What are M, A and P.

According to the video, the book Tiny Habits by BJ Fogg talks about a three-part method to develop new habits easily: B = MAP, which stands for motivation (M) plus ability (A) plus prompt (P).

- **Motivation (M):** This refers to your desire or willingness to do something.
- **Ability (A):** This refers to your capacity to perform a behavior.
- **Prompt (P):** This refers to a cue or trigger that reminds you to do something.

The key is to make the habit small and achievable. The smaller the behavior, the less motivation you'll need and the more likely you are to stick with it.

The second part is to identify an action prompt. This means pairing your new habit with something you already do.

The third part is to celebrate your wins. Even if you only do a small thing, you should take a moment to feel proud of yourself. Celebrating small wins increases your motivation and makes you more likely to repeat the behavior.

## Why it is important to "Shine" or Celebrate after each successful completion of habit?

Celebrating success after completing a habit reinforces the behavior in your brain. It's like saying "good job!" to yourself, which makes your brain want to do it again. So, celebrating after each success helps turn the behavior into a habit, this is how our mind work.

## The most interesting story from 1% Better Every Day By James Clear

According to the video, the most interesting idea was about how changing our perspective can transform our experiences. This means that small improvements can lead to remarkable results over time. The story about the man who saw his illness as a gift instead of a curse was really eye-opening. It showed how shifting our mindset can lead to personal growth and resilience.

##  Perspective about Identity from Atomic Habits Book by James Clear?

- Dynamic Identity: The book suggests that identity is not fixed it constantly evolves over time.
- Influenced by External Factors: It emphasizes how identity is shaped by external influences such as culture, society, and relationships.
- Intersectionality: It explores how different aspects of identity, like race, gender, and class, intersect and affect each other.
- Complexity: Identity is portrayed as fluid and complex, challenging the idea of simple categorization.

## Book's perspective on how to make a habit easier to do?

- Start Tiny: Begin with a tiny, manageable action related to the habit you want to develop.
- Stay Consistent: Regularly practice the habit, even in small increments, to integrate it into your routine effectively.
- Reflect and Learn: Take time to reflect on your progress and how the habit impacts your life, reinforcing its significance.
- Enjoy small wins: Celebrate even the smallest victories along your habit-building journey to maintain motivation.

## Book's perspective on how to make a habit harder to do?

- Increase Difficulty: Make the habit more challenging by adding obstacles or making it physically or mentally demanding.

- Change Environment: Change where you usually do the habit to somewhere less suitable, so it's not as easy to do.
- Delay It: Add delays or things in the way before you can do the habit, making it less instant.
- Negative Consequences: Make there be bad outcomes if you do the habit, so you're less likely to want to do it.

## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

### Habit: Reading more books

- Make it accessible: Put a book you want to read in a place you see often.
- Make the Habit More Attractive: Pick books about things you really like.
- Make the Habit Easier: Begin with short reading times and slowly do it for longer.
- Celebrate: Treat yourself after reading, like with a snack or break.


## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

Habit: Spending too much time on social media

- Make it unaccessible: Turn off notifications for social media apps or set specific times for checking social media.

- Make it Unattractive: Don't use any apps that seems to be addictive and think of negative consequences of it. Instead have some generic apps and try to stick to it.

- Make the Habit Harder: Keep your phone out of reach or in a silent mode during certain activities or for specific time every day.
- Shift the focus: Should think of spending time to some productive task instead of wasting on social medias.