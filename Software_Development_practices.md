# Good Practices for Software Development

## Which point(s) were new to you?

- Use of tools like Trello or Jira for documentation.
- Using Github gists to share code snippets.
- Sandbox environments like Codepen or Codesandbox for sharing setups
- Strategies for preserving attention and practicing deep work

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

#### proving over-communication and keeping team members informed
- Idea: Schedule regular check-ins or status updates to ensure everyone is on the same page
#### Enhancing communication skills when asking questions
- Idea: Practice framing questions clearly and providing relevant context
#### Managing distractions and maintaining focus during work hours
- Idea: Experiment with time management tools and techniques, such as setting specific work hours and minimizing distractions.