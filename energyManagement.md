# Energy Mangement

## What are the activities you do that make you relax - Calm quadrant?

To relax in the calm quadrant, I enjoy activities like reading a book, taking a walk outside, meditating, or simply listening to some calming music. These things help me unwind and feel refreshed.

## When do you find getting into the Stress quadrant?

I usually end up in the stress quadrant when I have tight deadlines, tough tasks, or when things don't go as planned. It's like when there's a lot to handle and I start feeling the pressure building up. But a little stress can sometimes motivate me to get things done.

## How do you understand if you are in the Excitement quadrant?

- Feeling eager and enthusiastic: When excited and can't wait to jump into something.
- Positive anticipation: When looking forward to what's coming with a sense of excitement.
- Increased energy levels: When feel energized and ready to take on challenges or new experiences.
- Sense of adventure: When curious to explore new things and unknown places.

## Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Sleep is crucial for our health and performance.
- Lack of sleep affects our brain function, making it harder to concentrate and remember things.
- During sleep, our brains consolidate memories and clean out toxins.
- Lack of sleep increases the risk of different health issues, including heart disease and obesity.
- A good sleep habit involves a consistent sleep schedule and creating a comfortable sleep environment.

## What are some ideas that you can implement to sleep better?

Here are some simple ideas to help sleep better:

- Maintain regular sleep time. One need to go to bed and wake up at the time fixed every day.

- Make sure bedroom is comfortable. Keep it cool, dark, and quiet for better sleep.

- Avoid screens like phones or computers before bed, as the light can make it harder to fall asleep.

- Don't eat heavy meals or drink caffeine close to bedtime, as they can keep awake.

- Try to relax and manage stress with deep breathing or meditation exercises.

- Make sure mattress and pillows are comfortable and supportive.

- Limit naps during the day, especially in the late afternoon or evening.

## Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

Here are five key points paraphrased from the video "Brain Changing Benefits of Exercise":

- Doing regular exercise is really good for your brain.
- When you exercise, your brain grows new cells, which makes you smarter.
- It also makes you feel happier by releasing chemicals that make you feel good.
- Exercise helps keep your brain healthy as you get older, lowering the chances of memory problems.
- Even a little bit of exercise, like going for a walk, can make a big difference for your brain.

## What are some steps you can take to exercise more?


- Start with small steps: Begin with short activities like a quick walk or stretching at home.

- Choose enjoyable activities. Pick exercises that i like, whether walking, dancing, or playing sports.

- Make it social: Exercise with friends or join a class to make it more fun and hold myself accountable.

- Fit it into routine: Schedule exercise like any other appointment, and find ways to be active during day, like taking the stairs or walking instead of driving.