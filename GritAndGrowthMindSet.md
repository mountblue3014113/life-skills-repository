# Grit and Growth Mindset

## Grit

The person in the video used to work a tough job but then became a teacher. They found out that being passionate and sticking with goals for a long time, called "grit," is really important for success. They studied this in schools and other places and found that kids with grit were more likely to do well.

## Introduction to Growth Mindset

The video talks about two ways people think about learning: a fixed mindset and a growth mindset. A fixed mindset is when people believe skills are fixed and can't change much. A growth mindset is when people believe they can improve their skills through effort and learning.

## Understanding Internal Locus of Control

#### Internal Locus of Control: 
It's the belief that you have control over your life and that the outcomes are influenced by your actions and efforts.

#### Key Points in the Video:

 - The study showed that students who were praised for their effort were more motivated and performed better.
- Having an internal locus of control, where you believe in your ability to influence outcomes through your efforts, leads to higher motivation and better performance.
- Examples from sales teams and personal experiences illustrate how having an internal locus of control leads to greater persistence.

## How to build a Growth Mindset

- Believe in your ability to figure things out and get better.
- Question your assumptions about your capabilities.
- Create your own life curriculum to learn and grow.
- Honor the struggle and use it as a tool for growth.

## What are your ideas to take action and build Growth Mindset?

- Take Responsibility: Acknowledge that your learning journey is your own responsibility. Don't rely solely on others.

- Persevere Through Challenges: When faced with tough problems, don't give up easily. Stay committed and work persistently until you find a solution.

- Value Effort: Understand that putting in more effort leads to better understanding. Embrace hard work as a necessary step for growth.

- Learn from Errors: Instead of feeling discouraged by mistakes, view them as opportunities for learning. Analyze what went wrong, and use that knowledge to improve your skills.