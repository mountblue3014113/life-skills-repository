# REST Architecture

Representational State Transfer **(REST)** is an architectural style for designing networked applications. It emphasizes scalability, reliability, and modifiability. RESTful systems utilize HTTP protocol for communication and are based on a client-server model where resources are identified by URIs. Here are some key points about REST architecture:

*  Client-Server Architecture: REST separates the concerns of client   and server, allowing them to evolve independently. This improves scalability and simplifies the overall architecture.
*  Statelessness: RESTful interactions are stateless, meaning each request from a client to a server must contain all the information necessary to understand and process the request. This enhances reliability and scalability.
* Uniform Interface: RESTful systems have a uniform interface, typically using HTTP methods like GET, POST, PUT, DELETE to perform CRUD (Create, Read, Update, Delete) operations on resources. This standardization simplifies the client-server communication.
*  Resource Identification: Resources in a RESTful system are identified by URIs (Uniform Resource Identifiers). Clients interact with these resources using standard HTTP methods.
*  Representation: Resources are represented in various formats such as JSON, XML, or HTML. Clients can request different representations of a resource using content negotiation.
* Stateless Communication: RESTful communication is stateless, meaning each request from a client to a server must contain all the information necessary to understand and process the request. This simplifies server implementation and improves scalability.
* Caching: REST supports caching to improve performance. Responses from the server can be cached by clients to reduce the need for repeated requests.
* Layered System: REST allows for the use of intermediary servers such as proxies and caches, which can improve scalability and security.


## References:
* [Fielding, R. T. (2000). Architectural Styles and the Design of Network-based Software Architectures.](https://ics.uci.edu/~fielding/pubs/dissertation/top.htm)

* [Richardson, L., & Ruby, S. (2007). RESTful Web Services. O'Reilly Media.](https://www.oreilly.com/library/view/restful-web-services/9780596529260/)

* ["RESTful Web Services - The Complete Guide" by Baeldung](https://www.baeldung.com/rest-with-spring-series)
* ["Understanding RESTful APIs" by IBM Developer](https://careers.ibm.com/job/20038712/be-broker-api-developer-buenos-aires-ar/)
